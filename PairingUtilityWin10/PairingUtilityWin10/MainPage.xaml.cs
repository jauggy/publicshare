﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using Windows.Devices.Enumeration;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Navigation;

// The Blank Page item template is documented at http://go.microsoft.com/fwlink/?LinkId=402352&clcid=0x409

namespace PairingUtilityWin10
{
    /// <summary>
    /// An empty page that can be used on its own or navigated to within a Frame.
    /// </summary>
    public sealed partial class MainPage : Page
    {

        public String FamilyName { get; set; }
        public static MainPage Current;
        public CustomDeviceWatcher BluetoothWatcher { get; set; }
        public CustomDeviceWatcher BluetoothLEWatcher { get; set; }


        public MainPage()
        {
            FamilyName = Windows.ApplicationModel.Package.Current.Id.FamilyName;
            BluetoothWatcher = new CustomDeviceWatcher(DeviceSelectorChoices.Bluetooth);
            BluetoothLEWatcher = new CustomDeviceWatcher(DeviceSelectorChoices.BluetoothLE);

            this.Loaded += MainPage_Loaded;
            this.InitializeComponent();
            Current = this;
        }

        private void MainPage_Loaded(object sender, RoutedEventArgs e)
        {
            BluetoothWatcher.StartWatcher();
            BluetoothLEWatcher.StartWatcher();
        }

        protected override void OnNavigatedTo(NavigationEventArgs e)
        {
            var uri = e.Parameter.ToString();

            base.OnNavigatedTo(e);
        }

        private void resultsListView_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            UpdatePairingButtons();
        }

        private void UpdatePairingButtons()
        {
            DeviceInformationDisplay deviceInfoDisp = (DeviceInformationDisplay)resultsListView.SelectedItem;

            if (null != deviceInfoDisp &&
                deviceInfoDisp.DeviceInformation.Pairing.CanPair &&
                !deviceInfoDisp.DeviceInformation.Pairing.IsPaired)
            {
                pairButton.IsEnabled = true;
            }
            else
            {
                pairButton.IsEnabled = false;
            }

            if (null != deviceInfoDisp &&
                deviceInfoDisp.DeviceInformation.Pairing.IsPaired)
            {
                unpairButton.IsEnabled = true;
            }
            else
            {
                unpairButton.IsEnabled = false;
            }
        }

        private void classicResultsListView_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
           
        }

        private void classicResultsListView_ItemClick(object sender, ItemClickEventArgs e)
        {
            resultsListView.SelectedIndex = -1;
            SetPairButtonsBasedOnSelectedItem();
        }

        /// <summary>
        /// Click Low Energy Bluetooth results
        /// </summary>
        private void resultsListView_ItemClick(object sender, ItemClickEventArgs e)
        {
            classicResultsListView.SelectedIndex = -1;
            SetPairButtonsBasedOnSelectedItem();
        }

        private async void pairButton_Click(object sender, RoutedEventArgs e)
        {
            resultsListView.IsEnabled = false;
            classicResultsListView.IsEnabled = false;

            pairButton.IsEnabled = false;

            message.Text = "Pairing started. Please wait...";

            DeviceInformationDisplay deviceInfoDisp = resultsListView.SelectedItem as DeviceInformationDisplay;
            DevicePairingResult dupr = await deviceInfoDisp.DeviceInformation.Pairing.PairAsync();

            message.Text = "Pairing result = " + dupr.Status.ToString();
            resultsListView.IsEnabled = true;
            classicResultsListView.IsEnabled = true;
        }

        private void SetControlsBusy(bool busy)
        {
            resultsListView.IsEnabled = !busy;
            classicResultsListView.IsEnabled = !busy;

            if (busy)
            {
                unpairButton.IsEnabled = !busy;
                pairButton.IsEnabled = !busy;
            }
            else
            {
                SetPairButtonsBasedOnSelectedItem();
            }


        }

        private void SetPairButtonsBasedOnSelectedItem()
        {
            DeviceInformationDisplay deviceInfoDisp = GetSelectedDevice();
            if(deviceInfoDisp==null)
            {
                pairButton.IsEnabled = false;
                unpairButton.IsEnabled = false;
            }
            else if (deviceInfoDisp.CanPair)
            {
                pairButton.IsEnabled = true;
                unpairButton.IsEnabled = false;
            }
            else
            {
                pairButton.IsEnabled = false;
                unpairButton.IsEnabled = true;
            }
        }

        private DeviceInformationDisplay GetSelectedDevice()
        {
            DeviceInformationDisplay leDevice = resultsListView.SelectedItem as DeviceInformationDisplay;
            DeviceInformationDisplay classicDevice = classicResultsListView.SelectedItem as DeviceInformationDisplay;

            if (leDevice != null)
                return leDevice;
            if (classicDevice != null)
                return classicDevice;

            return null;
        }



        private async void unpairButton_Click(object sender, RoutedEventArgs e)
        {
            SetControlsBusy(true);
            message.Text = "Unpairing started. Please wait...";
            var device = GetSelectedDevice();
            DeviceUnpairingResult dupr = await device.DeviceInformation.Pairing.UnpairAsync();
            message.Text = "Unpairing result = " + dupr.Status.ToString();
            SetControlsBusy(false);
        }
    }
}
